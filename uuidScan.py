import asyncio
from bleak import BleakScanner, BleakClient

async def discover_device_services(mac_address):
    async with BleakClient(mac_address) as client:
        services = await client.get_services()
        for service in services:
            print(f"Service UUID : {service.uuid}")
            for char in service.characteristics:
                print(f"  - Caractéristique UUID : {char.uuid}")
                try: 
                    value = await client.read_gatt_char(char.uuid)
                    print(f"Valeur de la caractéristique : {value}")
                except:
                    print("Valeur de caracteristique non permise")
                print("--------------------------------------------------------------")

if __name__ == "__main__":
    target_mac_address = "E9:DF:38:FC:46:F4"
    
    loop = asyncio.get_event_loop()
    loop.run_until_complete(discover_device_services(target_mac_address))
