import asyncio
from bleak import BleakClient

async def notification_handler(sender, data):
    print(f"Notification reçue de {sender}: {data}")

async def start_notification(mac_address, characteristic_uuid):
    async with BleakClient(mac_address) as client:
        try:
            await client.start_notify(characteristic_uuid, notification_handler)
            print(f"Notification démarrée sur la caractéristique {characteristic_uuid}...")
            await asyncio.sleep(600)  # Vous pouvez ajuster la durée de notification
        except Exception as e:
            print(f"Erreur lors du démarrage de la notification : {e}")

if __name__ == "__main__":
    target_mac_address = "E9:DF:38:FC:46:F4"
    target_characteristic_uuid = "1b0d1407-a720-f7e9-46b6-31b601c4fca1" 
    
    loop = asyncio.get_event_loop()
    loop.run_until_complete(start_notification(target_mac_address, target_characteristic_uuid))
