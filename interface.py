import asyncio
from bleak import BleakScanner, BleakClient
import sys

async def scan_ble_devices():
    dict_n_addr = {}
    devices = await BleakScanner.discover()
    for device in devices:
        dict_n_addr[device.name] = device.address
    return dict_n_addr


async def discover_device_services(mac_address):
    async with BleakClient(mac_address) as client:
        services = await client.get_services()
        for service in services:
            print(f"Service UUID : {service.uuid}")
            for char in service.characteristics:
                print(f"  - Caractéristique UUID : {char.uuid}")
                try: 
                    value = await client.read_gatt_char(char.uuid)
                    print(f"Valeur de la caractéristique : {value}")
                except:
                    print("Valeur de caracteristique non permise")
                print("--------------------------------------------------------------")

async def notification_handler(sender, data):
    print(f"Notification reçue de {sender}: {data}")

async def start_notification(mac_address, characteristic_uuid):
    async with BleakClient(mac_address) as client:
        try:
            await client.start_notify(characteristic_uuid, notification_handler)
            print(f"Notification démarrée sur la caractéristique {characteristic_uuid}...")
            await asyncio.sleep(600)  
        except Exception as e:
            print(f"Erreur lors du démarrage de la notification : {e}")

async def main():

    if sys.argv[1] == "h":
        print("h : Affiche un menu d'aide")
        print("imac: affiche la liste des interfaces bluetooth à proximités")
        print("[nom de l'interface] s: Effectue un scan UUID et caracteristiques")
        print("notify [MAC] [CARACTERISTIQUEUUID]: Lance un notify sur la caracteristique à une adresse mac donnée.")
        print("notify [MAC] all: Lance un notify sur toute les caracteristiques à une adresse mac donnée.")
        print("send [MAC] [CARACTERISTIQUEUUID] envoie de donnée à une adresse mac donnée.")

    elif sys.argv[1] == "imac":
        devices_dict = await scan_ble_devices()
        for name, address in devices_dict.items():
            print(f"Nom: {name}, Adresse: {address}")
    elif sys.argv[1] == "notify":
        await start_notification(sys.argv[2], sys.argv[3])
    else:
        devices_dict = await scan_ble_devices()
        interface_name = sys.argv[1]
        try:
            interface_mac = devices_dict[interface_name]
        except:
            print(f"Erreur, Appareil {sys.argv[1]} n'existe pas")
            return
    
        if sys.argv[2] == 's':
            await discover_device_services(interface_mac)


    
    #await discover_device_services(interface_mac)
    

if __name__ == "__main__":
    asyncio.run(main())
