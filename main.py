import asyncio
from bleak import BleakScanner

async def scan_ble_devices():
    devices = await BleakScanner.discover()
    for device in devices:
        print(f"Nom: {device.name}, Adresse: {device.address}")

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(scan_ble_devices())

#L'adresse MAC de Nom: OBU-8671, Adresse: E9:DF:38:FC:46:F4